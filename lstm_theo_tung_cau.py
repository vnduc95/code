# -*- coding: utf-8 -*-

from __future__ import print_function
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation
from keras.layers import LSTM
from keras.optimizers import RMSprop
from keras.utils.data_utils import get_file
import numpy as np
import random
import sys

# read from data file
sentences = open("./Data1/0").read().lower().split('.')
print("So cau: ", len(sentences))

text = open("chars_part_3").read().lower()
text = text.split()

chars = sorted(list(set(text)))
chars.append('')
print('total chars:', len(chars))
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

historys = []
next_chars = []
maxlen = 23

# ham convert tu 1 mang thanh 1 mang co len = l
def convert(arr, l):
    result = [];
    for i in range(l):
        result.append("")

    for i in xrange(0, len(arr)):
        result[l - len(arr) + i] = arr[i]
    return result

def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    # print (sorted(preds, reverse=False))
    # print(preds[char_indices['tôi']])
    probas = np.random.multinomial(1, preds, 1)
    # print(probas)
    # probas = sorted(probas)
    # temp = np.asarray(probas).tolist()
    # for i in range (len(temp)):
    #   if temp[i]>0:
    #       print (temp[i])
    # print (temp[1])

    for i in range (15):
        print (indices_char[np.argmax(preds)])
        preds[np.argmax(preds)]=0;
    return np.argmax(preds)

# tao historys va next_chars tu tap du lieu
for x in xrange(0, len(sentences)):
    words = sentences[x].split()
    if (len(words) < maxlen) and (len(words) > 1):
        for i in xrange(1, len(words)):
            historys.append(convert(words[0: i], maxlen))
            next_chars.append(words[i])



# ================================ TRAINING ============================


print('Vectorization...')
X = np.zeros((len(historys), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(historys), len(chars)), dtype=np.bool)
for i, history in enumerate(historys):
    for t, char in enumerate(history):
        X[i, t, char_indices[char]] = 1

    y[i, char_indices[next_chars[i]]] = 1

# build the model: a single LSTM
print('Build model...')
model = Sequential()
model.add(LSTM(128, input_shape=(maxlen, len(chars))))
model.add(Dense(len(chars)))
model.add(Activation('softmax'))

optimizer = RMSprop(lr=0.01)
model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

# load model
model = load_model("./model")

# training
# for iteration in range(1, 50):
#     print()
#     print('-' * 50)
#     print('Iteration', iteration)
#     model.fit(X, y,
#               batch_size=128,
#               epochs=1)

#     model.save("./model123".format(iteration))

# ================================ END TRAINING ============================




# ================================ TEST ====================================

diversity = 1
while (True):
    generated = ''
    print('Nhap cau: ')
    # sentence = ' '.join(text[start_index: start_index + maxlen])
    # sentence = 'nhong nhong ăn bám vào bố mẹ thì chỉ sinh ra tính ỷ lại xấu'
    sentence = raw_input()
    sentence = sentence.lower()
    generated += sentence

    sys.stdout.write('=>   ' + generated)

    x = np.zeros((1, maxlen, len(chars)))
    for i in range(maxlen - len(sentence.split())):
        x[0, i, char_indices[""]] = 1.
    
    for t, char in enumerate(sentence.split()):
        x[0, maxlen - len(sentence.split()) + t, char_indices[char]] = 1.

    preds = model.predict(x, verbose=0)[0]
    # print (preds)
    next_index = sample(preds, diversity)
    next_char = indices_char[next_index]

    generated += next_char
    sentence = ' '.join(sentence.split()[1:] + [next_char])

    sys.stdout.write(' + ' + next_char + '\n\n\n')
    sys.stdout.flush()
    print()

# ================================ END TEST ================================