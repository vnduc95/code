# -*- coding: utf-8 -*-

from __future__ import print_function
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation
from keras.layers import LSTM
from keras.optimizers import RMSprop
from keras.utils.data_utils import get_file
import numpy as np
import random
import re
import sys

from tao_tap_nham_lan_am_tiet import tao_tap_nham_lan

# tap tu don tu dien
text = open("chars_part_3").read().lower()
text = text.split()

chars = sorted(list(set(text)))
chars.append('')

char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

maxlen = 23

def sample(preds, word):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds)
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)

    arr_word = tao_tap_nham_lan(word)
    max_probability = 0
    index_of_true_word = -1
    for i in xrange(len(arr_word)):
        if (arr_word[i] in chars) and preds[char_indices[ arr_word[i] ]] > max_probability:
            max_probability = preds[char_indices[ arr_word[i] ]]
            index_of_true_word = char_indices[ arr_word[i] ]

    # return np.argmax(preds)
    return index_of_true_word


# load model
model = load_model("./model")


# ================================ TEST ====================================

while (True):
    print('Nhap cau: ')
    sentence = raw_input()
    sentence = sentence.lower()

    words = sentence.split()

    for i in xrange(1,len(words)):
        sen = words[:i]
        index = i
        word_need_check = words[i]

        x = np.zeros((1, maxlen, len(chars)))
        for i in range(maxlen - len(sen)):
            x[0, i, char_indices[""]] = 1.
        
        for t, char in enumerate(sen):
            x[0, maxlen - len(sen) + t, char_indices[char]] = 1.

        preds = model.predict(x, verbose=0)[0]
        # print (preds)
        next_index = sample(preds, word_need_check)
        next_char = indices_char[next_index]

        words[index] = next_char
        print(word_need_check + " => " + next_char)

    sys.stdout.flush()
    print()

# ================================ END TEST ================================